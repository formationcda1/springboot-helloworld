package com.simplon.helloworld;

import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import ch.qos.logback.classic.Logger;

@SpringBootApplication
	//Commande Line : interface de Spring qui te permet d’exécuter du code au démarrage de l’application
public class HelloworldApplication  implements CommandLineRunner{
	//Le Logger permet d’afficher des messages dans la console ou de les enregistrer dans un fichier de log de l'app. Est associée à la classe HelloworldApplication
private static Logger LOG = (Logger) LoggerFactory.getLogger(HelloworldApplication.class);

	public static void main(String[] args) {
		//LOG.info(): façon dont on affiche des messages dans la console
		LOG.info("DÉMARRAGE DE L'APPLICATION");
        SpringApplication.run(HelloworldApplication.class, args);
        LOG.info("APPLICATION TERMINÉE");
	}
	@Override
	//Cette méthode est appelée automatiquement après le démarrage de l’application
	public void run(String... args)  {
		LOG.info("EXÉCUTION : command line runner");
        LOG.info("Hello World!");
	}

}
